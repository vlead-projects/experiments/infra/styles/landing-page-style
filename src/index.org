#+TITLE: Index to styles
#+AUTHOR: VLEAD
#+DATE: [2018-07-25 Wed]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This is an index to the realization of various styles for
  the experiments. 

* Experiment landing page
  Refer [[./runtime/index.org][index.org]] for more details
